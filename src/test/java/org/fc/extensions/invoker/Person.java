package org.fc.extensions.invoker;

public class Person {
	private String personName;
	private String personId;

	public String getPersonName() {
		return personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getPersonId() {
		return personId;
	}

	public void setPersonId(String personId) {
		this.personId = personId;
	}

	public String toString() {
		return "personName : " + personName + ", personId : " + personId;
	}
	
}
