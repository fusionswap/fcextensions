package org.fc.extensions.invoker;

import java.util.ArrayList;

import org.fc.extensions.GroovyCodeExecutor;
import org.fc.extensions.params.ExtendibleMethodParams;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PersonServiceTest {

	private static final Logger LOG = LoggerFactory.getLogger(GroovyCodeExecutor.class);
	
	@Test
	public void testExtensionWithArgModification() throws Exception {

		Person p = new Person();
		p.setPersonId("p1");
		p.setPersonName("person1 name");
		
		PersonService pService = new PersonService();
		
		ArrayList<ExtendibleMethodParams> args = new ArrayList<>();

		ExtendibleMethodParams arg1 = new ExtendibleMethodParams("p", Person.class, p);
		ExtendibleMethodParams arg2 = new ExtendibleMethodParams("changedName", String.class, "person changed name");
		args.add(arg1);
		args.add(arg2);
		
		ExtensionsInvoker<PersonService, Person> invoker = new GroovyExtensionsInvoker<PersonService, Person>(args, "changeName", pService,
				"p.personId = \"personInBefore1\"", "p.personId = \"personInAfter1\"; return p ");
		invoker.invokeBefore();
		LOG.debug("person from before -- " + p);
		Assert.assertEquals("personInBefore1", p.getPersonId());
		Assert.assertEquals("person1 name", p.getPersonName());
		
		
		Person retFromInvoke = invoker.invoke();
		LOG.debug("person from invoke -- " + p);
		Assert.assertEquals("personInBefore1", p.getPersonId());
		Assert.assertEquals("person changed name", p.getPersonName());
		
		
		Person retFromAfter = invoker.invokeAfter();
		LOG.debug("person from after -- " + p);
		Assert.assertEquals("personInAfter1", p.getPersonId());
		Assert.assertEquals("person changed name", p.getPersonName());
		
		
	}

}