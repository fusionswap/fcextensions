package org.fc.extensions.invoker;

public interface ExtensionsInvoker<O, R> {

	public void invokeBefore();
	public R invoke() throws Exception;
	public R invokeAfter();
}
