package org.fc.extensions.invoker;

import java.util.ArrayList;

import org.fc.extensions.GroovyCodeExecutor;
import org.fc.extensions.params.ExtendibleMethodParams;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GroovyCodeExecutorTest {

	private static final Logger LOG = LoggerFactory.getLogger(GroovyCodeExecutor.class);
	
	@Test
	public void testExtension() throws Exception {

		GroovyCodeExecutorTestObject obj = new GroovyCodeExecutorTestObject();

		ArrayList<ExtendibleMethodParams> args = new ArrayList<>();

		ExtendibleMethodParams arg1 = new ExtendibleMethodParams("i", Integer.class, 6);
		ExtendibleMethodParams arg2 = new ExtendibleMethodParams("j", Integer.class, 2);

		args.add(arg1);
		args.add(arg2);

		ExtensionsInvoker<GroovyCodeExecutorTestObject, Integer> invoker = new GroovyExtensionsInvoker<GroovyCodeExecutorTestObject, Integer>(args, "invokeMethod", obj,
				"return i+j", "return i-j");
		invoker.invokeBefore();
		Integer retFromInvoke = invoker.invoke();
		LOG.debug("retFromInvoke " + retFromInvoke);
		
		Integer retFromAfter = invoker.invokeAfter();
		LOG.debug("retFromAfter " + retFromAfter);
		
		
		Assert.assertEquals(3, retFromInvoke.intValue());
		Assert.assertEquals(4, retFromAfter.intValue());
		
	}


}