package org.fc.extensions.invoker;

public class PersonService {

	public Person changeName(Person p, String changedName) {
		p.setPersonName(changedName);
		return p;
	}

}
