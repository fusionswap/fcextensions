package org.fc.extensions;

import java.util.List;

import org.fc.extensions.params.ExtendibleMethodParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import groovy.lang.Script;

public class GroovyCodeExecutor<T> {
	
	private static final Logger LOG = LoggerFactory.getLogger(GroovyCodeExecutor.class);
	
	public T executeCode(String groovyCode, List<ExtendibleMethodParams> args) {
		LOG.debug("Executing script " + groovyCode);
		GroovyShell shell = new GroovyShell();
		Script script = shell.parse(groovyCode);
		Binding b = new Binding();
		for(ExtendibleMethodParams param : args) {
			b.setVariable(param.getArgName(), param.getArg());
		}
		script.setBinding(b);
		T returnVal = (T) script.run();
		return returnVal;
	}

}
