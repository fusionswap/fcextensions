package org.fc.extensions.invoker;

import java.lang.reflect.Method;
import java.util.ArrayList;

import org.fc.extensions.GroovyCodeExecutor;
import org.fc.extensions.params.ExtendibleMethodParams;


public class GroovyExtensionsInvoker<O, R> implements ExtensionsInvoker<O,R> {

	private ArrayList<ExtendibleMethodParams> methodParams;
	private String extendedMethodName;
	private O extendedObject;
	private String beforeScript;
	private String afterScript;
	GroovyCodeExecutor<R> codeExecutor = new GroovyCodeExecutor<>();

	public GroovyExtensionsInvoker(ArrayList<ExtendibleMethodParams> methodParams, String extendedMethodName, O extendedObject,
			String beforeScript, String afterScript) {
		super();
		this.methodParams = methodParams;
		this.extendedMethodName = extendedMethodName;
		this.extendedObject = extendedObject;
		this.beforeScript = beforeScript;
		this.afterScript = afterScript;
	}

	@Override
	public void invokeBefore() {
		codeExecutor.executeCode(beforeScript, methodParams);
	}

	@Override
	public R invoke() throws Exception {
		Class<?>[] argClasses = new Class<?>[methodParams.size()];
		Object[] args = new Object[methodParams.size()];
		
		int i = 0;
		for(ExtendibleMethodParams param : methodParams) {
			argClasses[i] = param.getArgClass();
			args[i] = param.getArg();
			i++;
		}
		
		Method m = extendedObject.getClass().getMethod(extendedMethodName, argClasses);		
		return (R) m.invoke(extendedObject, args);
	}

	@Override
	public R invokeAfter() {
		return codeExecutor.executeCode(afterScript, methodParams);
	}

}
