package org.fc.extensions.params;

public class ExtendibleMethodParams {
	private String argName;
	private Class argClass;
	private Object arg;

	public ExtendibleMethodParams(String argName, Class argClass, Object arg) {
		super();
		this.argName = argName;
		this.argClass = argClass;
		this.arg = arg;
	}

	public String getArgName() {
		return argName;
	}

	public void setArgName(String argName) {
		this.argName = argName;
	}

	public Class getArgClass() {
		return argClass;
	}

	public void setArgClass(Class argClass) {
		this.argClass = argClass;
	}

	public Object getArg() {
		return arg;
	}

	public void setArg(Object arg) {
		this.arg = arg;
	}

}
