package org.fc.extensions;

import java.util.ArrayList;
import java.util.List;

import org.fc.extensions.params.ExtendibleMethodParams;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GroovyCodeExecutorTest {

	private static final Logger LOG = LoggerFactory.getLogger(GroovyCodeExecutorTest.class);
	
	@Test
	public void testCodeExecution() throws Exception {
		String code = "return i*j";
		
		GroovyCodeExecutor<Integer> e = new GroovyCodeExecutor<>();
		List<ExtendibleMethodParams> args = new ArrayList<>();
		
		ExtendibleMethodParams arg1 = new ExtendibleMethodParams("i",Integer.class, 2);
		ExtendibleMethodParams arg2 = new ExtendibleMethodParams("j",Integer.class, 3);
		
		args.add(arg1);
		args.add(arg2);
		
		Integer o = e.executeCode(code, args);
		LOG.debug("returned value " + o);
		Assert.assertEquals(6, o.intValue());
	}
	
	
}
